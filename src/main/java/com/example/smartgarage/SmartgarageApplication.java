package com.example.smartgarage;

import com.example.smartgarage.models.ServiceStatus;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartgarageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartgarageApplication.class, args);
    }

}
