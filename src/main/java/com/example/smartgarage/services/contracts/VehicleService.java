package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.Visit;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface VehicleService {
    List<Vehicle> getAll(Pageable pageable, Map<String, String> params);
    Vehicle getById(int id);
    void create(Vehicle vehicle);
    void delete(Vehicle vehicle);
    void deleteAllByUser(User user);
    void update(Vehicle vehicle);
    List<Vehicle> search(Pageable pageable, String keyword);
}
