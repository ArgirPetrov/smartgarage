package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Service;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface ServiceService {
    Service getById(int id);
    void create(Service service);
    void update(Service service);
    boolean serviceExist(String serviceName);
    void delete(Service service);
    List<Service> getAll(Pageable pageable, Map<String, String> params);
    List<Service> search(Pageable pageable, String keyword);
    Service getByName(String serviceName);
}
