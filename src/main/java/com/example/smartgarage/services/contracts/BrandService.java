package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Brand;
import com.example.smartgarage.models.Model;

public interface BrandService {
    Brand createBrandIfNotExist(String brand);
}
