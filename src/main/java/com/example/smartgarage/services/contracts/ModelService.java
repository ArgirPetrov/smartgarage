package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Model;

public interface ModelService {
    Model createModelIfNotExit(String model, String brand);
}
