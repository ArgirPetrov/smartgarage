package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitServiceRelation;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface VisitService {
    void create(Visit visit);
    void update(Visit visit);
    Visit getById(int id);
    Visit getVisitByIdAndVehicleId(int vehicleId, int visitId);
    void addServices(Visit visitFromRepo, Set<VisitServiceRelation> visitServiceRelations);
    void removeServices(Visit visitFromRepo, Set<VisitServiceRelation> visitServiceRelations);
    List<Visit> getAll(Pageable pageable, Map<String, String> params);
}
