package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitServiceRelation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitServiceRelationService {
    void delete(VisitServiceRelation visitServiceRelation);
    VisitServiceRelation getByVisitAndServiceId(Visit visit, int serviceId);

    void update(VisitServiceRelation visitServiceRelation);
}
