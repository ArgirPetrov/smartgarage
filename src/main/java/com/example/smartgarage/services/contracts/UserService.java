package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.List;
import java.util.Map;

public interface UserService {
    List<User> getAll(Pageable pageable, Map<String, String> params);
    List<User> search(Pageable pageable, String keyword);
    User getById(int id);
    void create(User user);
    void delete(User user);
    void update(User user);

    User getByUsername(String username);
}
