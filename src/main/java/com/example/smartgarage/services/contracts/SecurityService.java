package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Vehicle;

public interface SecurityService {

    boolean isOwner(int vehicleId);
    boolean isOwner(Vehicle vehicle);
}
