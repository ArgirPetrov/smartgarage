package com.example.smartgarage.services;

import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.models.Brand;
import com.example.smartgarage.models.Model;
import com.example.smartgarage.repositories.ModelRepository;
import com.example.smartgarage.services.contracts.BrandService;
import com.example.smartgarage.services.contracts.ModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ModelServiceImpl implements ModelService {
    private final BrandService brandService;
    private final ModelRepository modelRepository;

    @Override
    public Model createModelIfNotExit(String model, String brand) {
        Brand brandObject = brandService.createBrandIfNotExist(brand);
        try {
            return modelRepository.getByBrandAndName(brandObject, model);
        } catch (EntityNotFoundException e) {
            Model modelObject = new Model(model, brandObject);
            modelRepository.save(modelObject);
            return modelObject;
        }
    }
}
