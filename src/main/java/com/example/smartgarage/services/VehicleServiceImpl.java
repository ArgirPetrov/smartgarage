package com.example.smartgarage.services;

import com.example.smartgarage.exceptionss.EntityDuplicateException;
import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.helpers.filteringandsorting.FilteringHelpers;
import com.example.smartgarage.helpers.filteringandsorting.VehicleFilterSortingRecord;
import com.example.smartgarage.models.CustomUserDetails;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.repositories.VehicleRepository;
import com.example.smartgarage.services.contracts.SecurityService;
import com.example.smartgarage.services.contracts.UserService;
import com.example.smartgarage.services.contracts.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {
    private final VehicleRepository vehicleRepository;
    @Override
    public List<Vehicle> getAll(Pageable pageable, Map<String, String> params) {
        VehicleFilterSortingRecord record = VehicleFilterSortingRecord
                .createVehicleFilterSortingRecord(params, pageable);
        return vehicleRepository.getAll(pageable, record.model(), record.brand(), record.year(),
                record.ownerUsername());
    }
    @Override
    public List<Vehicle> search(Pageable pageable, String keyword) {
        keyword = FilteringHelpers.modifySearchKeyword(keyword);
        pageable = FilteringHelpers.generateBasicPageable(pageable);
        return vehicleRepository.search(pageable, keyword);
    }
    @Override
    public Vehicle getById(int id) {
        return vehicleRepository.getById(id);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void create(Vehicle vehicle) {
        checkForDuplicatesAndSave(vehicle);
    }
    @Override
    @PreAuthorize("@securityService.isOwner(#vehicle) or hasRole('ADMIN')")
    public void update(Vehicle vehicle) {
        checkForDuplicatesAndSave(vehicle);
    }

    @Override
    @PreAuthorize("@securityService.isOwner(#vehicle) or hasRole('ADMIN')")
    public void delete(Vehicle vehicle) {
        vehicleRepository.delete(vehicle);
    }

    @Override
    public void deleteAllByUser(User user) {
        vehicleRepository.deleteAllByOwner(user);
    }

    private void checkForDuplicatesAndSave(Vehicle vehicle) {
        checkDuplicateVIN(vehicle);
        checkDuplicateLicensePlate(vehicle);
        vehicleRepository.save(vehicle);
    }

    private void checkDuplicateLicensePlate(Vehicle vehicle) {
        boolean duplicate = true;
        try {
            Vehicle vehicleFromRepo = vehicleRepository.getByLicensePlate(vehicle.getLicensePlate());
            if (vehicleFromRepo.getId() == vehicle.getId()) {
                duplicate = false;
            }
        } catch (EntityNotFoundException e) {
            duplicate = false;
        }
        if (duplicate) {
            throw new EntityDuplicateException("Vehicle", "license plate", vehicle.getLicensePlate());
        }
    }

    private void checkDuplicateVIN(Vehicle vehicle) {
        boolean duplicate = true;
        try {
            Vehicle vehicleFromRepo = vehicleRepository.getByVin(vehicle.getVin());
            if (vehicleFromRepo.getId() == vehicle.getId()) {
                duplicate = false;
            }
        } catch (EntityNotFoundException e) {
            duplicate = false;
        }
        if (duplicate) {
            throw new EntityDuplicateException("Vehicle", "VIN", vehicle.getVin());
        }
    }
}
