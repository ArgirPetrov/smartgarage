package com.example.smartgarage.services;

import com.example.smartgarage.exceptionss.EntityDuplicateException;
import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.helpers.filteringandsorting.FilteringHelpers;
import com.example.smartgarage.helpers.filteringandsorting.UserFilterSortingRecord;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.repositories.UserRepository;
import com.example.smartgarage.services.contracts.UserService;
import com.example.smartgarage.services.contracts.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    public static final int USER_PASSWORD_MAX_LENGTH = 20;
    private final UserRepository userRepository;
    private final VehicleService vehicleService;
    private final PasswordEncoder encoder;
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getAll(Pageable pageable, Map<String, String> params) {
        UserFilterSortingRecord record = UserFilterSortingRecord.createUserFilterSortingRecord(params, pageable);
         return userRepository.getAll(record.pageable(), record.username(), record.phoneNumber(), record.email(),
                 record.makeOrModel(), record.visitBegin(), record.visitEnd());
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> search(Pageable pageable, String keyword) {
        keyword = FilteringHelpers.modifySearchKeyword(keyword);
        pageable = FilteringHelpers.generateBasicPageable(pageable);
        return userRepository.search(pageable, keyword);
    }

    @Override
    @PostAuthorize("#id == authentication.principal.getUser().getId() or hasRole('ADMIN')")
    public User getById(int id) {
        User user = userRepository.getById(id);
        checkIfUserDeleted(user);
        return user;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void create(User user) {
        checkDuplicateEmail(user);
        checkDuplicateUsername(user);
        checkDuplicatePhoneNumber(user);

        encodePasswordAndSave(user);
    }

    private void checkDuplicatePhoneNumber(User user) {
        boolean duplicate = true;
        try {
            User userFromRepo = userRepository.getByPhoneNumber(user.getPhoneNumber());
            if (userFromRepo.getId() == user.getId()) {
                duplicate = false;
            }
        } catch (EntityNotFoundException e) {
            duplicate = false;
        }
        if (duplicate) {
            throw new EntityDuplicateException("User", "phone number", String.valueOf(user.getPhoneNumber()));
        }
    }
    @Override
    @PreAuthorize("hasRole('ADMIN') or #user == authentication.principal.getUser()")
    public void delete(User user) {
        user.setDeleted(true);
        vehicleService.deleteAllByUser(user);
        user.setVehicles(null);
        userRepository.save(user);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') or #user == authentication.principal.getUser()")
    public void update(User user) {
        encodePasswordAndSave(user);
    }

    private static void checkIfUserDeleted(User user) {
        if (user.isDeleted()) {
            throw new EntityNotFoundException("User", user.getId());
        }
    }

    private void checkDuplicateUsername(User user) {
        boolean duplicate = true;
        try {
            User userFromRepo = userRepository.getByUsername(user.getUsername());
            if (userFromRepo.getId() == user.getId()) {
                duplicate = false;
            }
        } catch (EntityNotFoundException e) {
            duplicate = false;
        }
        if (duplicate) {
            throw new EntityDuplicateException("User", "username", user.getUsername());
        }
    }

    private void checkDuplicateEmail(User user) {
        boolean duplicate = true;
        try {
            User userFromRepo = userRepository.getByEmail(user.getEmail());
            if (userFromRepo.getId() == user.getId()) {
                duplicate = false;
            }
        } catch (EntityNotFoundException e) {
            duplicate = false;
        }
        if (duplicate) {
            throw new EntityDuplicateException("User", "email", user.getEmail());
        }
    }

    private void encodePasswordAndSave(User user) {
        String userPassword = user.getPassword();
        if (userPassword.length() <= USER_PASSWORD_MAX_LENGTH) user.setPassword(encoder.encode(userPassword));
        userRepository.save(user);
        user.setPassword(userPassword);
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.getByUsername(username);
        checkIfUserDeleted(user);
        return user;
    }
}
