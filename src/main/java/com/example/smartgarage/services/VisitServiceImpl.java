package com.example.smartgarage.services;

import com.example.smartgarage.exceptionss.EntityDuplicateException;
import com.example.smartgarage.helpers.filteringandsorting.VisitFilterSortingRecord;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitServiceRelation;
import com.example.smartgarage.repositories.VisitRepository;
import com.example.smartgarage.services.contracts.VehicleService;
import com.example.smartgarage.services.contracts.VisitService;
import com.example.smartgarage.services.contracts.VisitServiceRelationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class VisitServiceImpl implements VisitService {
    private final VisitRepository visitRepository;
    private final VehicleService vehicleService;
    private final VisitServiceRelationService visitServiceRelationService;
    @Override
    public List<Visit> getAll(Pageable pageable, Map<String, String> params) {
        VisitFilterSortingRecord record = VisitFilterSortingRecord
                .createVisitFilterSortingRecord(params, pageable);
        return visitRepository.getAll(record.userId(), record.vehicleId(), record.visitBegin(), record.visitEnd(),
                record.pageable());
    }
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void create(Visit visit) {
        checkForDuplicateVisit(visit);
        visitRepository.save(visit);
    }

    private void checkForDuplicateVisit(Visit visit) {
        if (visit.getVehicle().getVisits().stream().anyMatch(v -> !v.isFinished())) {
            throw new EntityDuplicateException("There is another visit of this vehicle that is not finished!");
        }
    }
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void update(Visit visit) {
        visitRepository.save(visit);
    }

    @Override
    public Visit getById(int id) {
        return visitRepository.getById(id);
    }

    @Override
    public Visit getVisitByIdAndVehicleId(int vehicleId, int visitId) {
        Vehicle vehicle = vehicleService.getById(vehicleId);
        return visitRepository.getByIdAndVehicle(visitId, vehicle);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void addServices(Visit visitFromRepo, Set<VisitServiceRelation> visitServiceRelations) {
        checkIfFinished(visitFromRepo);
        visitServiceRelations.forEach(visitServiceRelation -> {
            if (!visitFromRepo.getServices().contains(visitServiceRelation)) {
                visitFromRepo.addVisitServiceRelation(visitServiceRelation);
                visitFromRepo.addToPrice(visitServiceRelation.getServicePrice());
            }
        });
        visitRepository.save(visitFromRepo);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void removeServices(Visit visitFromRepo, Set<VisitServiceRelation> visitServiceRelations) {
        checkIfFinished(visitFromRepo);
        visitServiceRelations.forEach(visitServiceRelation -> {
            if (visitFromRepo.getServices().contains(visitServiceRelation)) {
                visitFromRepo.removeFromPrice(visitServiceRelation.getServicePrice());
                visitFromRepo.removeVisitServiceRelation(visitServiceRelation);
                visitServiceRelationService.delete(visitServiceRelation);
            }
        });
        visitRepository.save(visitFromRepo);
    }

    private void checkIfFinished(Visit visitFromRepo) {
        if (visitFromRepo.isFinished()) {
            throw new AccessDeniedException("Visit is finished and you cannot add/remove services.");
        }
    }

}
