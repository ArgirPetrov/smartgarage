package com.example.smartgarage.services;

import com.example.smartgarage.exceptionss.EntityDuplicateException;
import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.helpers.filteringandsorting.FilteringHelpers;
import com.example.smartgarage.helpers.filteringandsorting.ServiceFilterSortingRecord;
import com.example.smartgarage.models.Service;
import com.example.smartgarage.repositories.ServiceRepository;
import com.example.smartgarage.services.contracts.ServiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Map;

@org.springframework.stereotype.Service
@RequiredArgsConstructor
public class ServiceServiceImpl implements ServiceService {
    private final ServiceRepository serviceRepository;
    @Override
    public List<Service> getAll(Pageable pageable, Map<String, String> params) {
        ServiceFilterSortingRecord record = ServiceFilterSortingRecord
                .createServiceFilterSortingRecord(params, pageable);
        return serviceRepository.getAll(record.name(), record.priceMin(), record.priceMax(), pageable);
    }
    @Override
    public List<Service> search(Pageable pageable, String keyword) {
        keyword = FilteringHelpers.modifySearchKeyword(keyword);
        pageable = FilteringHelpers.generateBasicPageable(pageable);
        return serviceRepository.search(pageable, keyword);
    }
    @Override
    public Service getById(int id) {
        Service service = serviceRepository.getById(id);
        checkIfDeleted(service);
        return service;
    }
    @Override
    public Service getByName(String serviceName) {
        Service service = serviceRepository.getByName(serviceName);
        checkIfDeleted(service);
        return service;
    }
    @Override
    public boolean serviceExist(String service) {
        return serviceRepository.existsByName(service);
    }
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void create(Service service) {
        checkForDuplicateAndSave(service);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void update(Service service) {
        checkForDuplicateAndSave(service);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(Service service) {
        service.setDeleted(true);
        serviceRepository.save(service);
    }

    private static void checkIfDeleted(Service service) {
        if (service.isDeleted()) {
            throw new EntityNotFoundException("Service", service.getId());
        }
    }

    private void checkForDuplicateAndSave(Service service) {
        duplicateServiceCheck(service);
        serviceRepository.save(service);
    }

    private void duplicateServiceCheck(Service service) {
        boolean duplicate = true;
        try {
            Service serviceFromRepo = serviceRepository.getByName(service.getName());
            if (service.getId() == serviceFromRepo.getId()) {
                duplicate = false;
            }
        } catch (EntityNotFoundException e) {
            duplicate = false;
        }
        if (duplicate) {
            throw new EntityDuplicateException("Service", "name", service.getName());
        }
    }
}
