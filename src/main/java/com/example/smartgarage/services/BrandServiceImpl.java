package com.example.smartgarage.services;

import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.models.Brand;
import com.example.smartgarage.models.Model;
import com.example.smartgarage.repositories.BrandRepository;
import com.example.smartgarage.repositories.ModelRepository;
import com.example.smartgarage.services.contracts.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService {
    private final BrandRepository brandRepository;
    @Override
    public Brand createBrandIfNotExist(String brandName) {
        try {
            return brandRepository.getByName(brandName);
        } catch (EntityNotFoundException e) {
            Brand brand = new Brand(brandName);
            brandRepository.save(brand);
            return brand;
        }
    }
}
