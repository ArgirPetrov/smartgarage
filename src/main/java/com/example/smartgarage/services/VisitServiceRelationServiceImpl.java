package com.example.smartgarage.services;

import com.example.smartgarage.models.ServiceStatus;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitServiceRelation;
import com.example.smartgarage.repositories.VisitServiceRelationRepository;
import com.example.smartgarage.services.contracts.VisitServiceRelationService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VisitServiceRelationServiceImpl implements VisitServiceRelationService {
    private final VisitServiceRelationRepository visitServiceRelationRepository;
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(VisitServiceRelation visitServiceRelation) {
        visitServiceRelationRepository.delete(visitServiceRelation);
    }

    @Override
    public VisitServiceRelation getByVisitAndServiceId(Visit visit, int serviceId) {
        return visitServiceRelationRepository.getByVisitAndServiceId(visit, serviceId);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void update(VisitServiceRelation visitServiceRelation) {
        visitServiceRelationRepository.save(visitServiceRelation);
    }
}
