package com.example.smartgarage.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "visits_services")
public class VisitServiceRelation {
    @EmbeddedId
    private VisitServiceId id;
    @Column(name = "original_service_name")
    private String serviceName;
    @Column(name = "original_service_price")
    private double servicePrice;
    @Enumerated(EnumType.STRING)
    @Generated(GenerationTime.INSERT)
    @Column(name = "service_status")
    private ServiceStatus status;
    @Generated(GenerationTime.ALWAYS)
    @Column(name = "date_created")
    private LocalDateTime dateCreated;
    public VisitServiceRelation(VisitServiceId id, String serviceName, double servicePrice) {
        this.id = id;
        this.servicePrice = servicePrice;
        this.serviceName = serviceName;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisitServiceRelation that = (VisitServiceRelation) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
