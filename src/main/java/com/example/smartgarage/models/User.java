package com.example.smartgarage.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
@SecondaryTable(name = "permissions", pkJoinColumns = @PrimaryKeyJoinColumn(name = "user_id"))
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String password;
    private String email;
    @Column(name = "phone_number")
    private long phoneNumber;
    @Column(table = "permissions", name = "is_employee")
    private boolean isEmployee;
    @Column(table = "permissions", name = "is_deleted")
    private boolean isDeleted;
    @Column(table = "permissions", name = "is_blocked")
    private boolean isBlocked;
    @org.hibernate.annotations.Generated(GenerationTime.ALWAYS)
    @Column(name = "date_created")
    private LocalDateTime dateCreated;
    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE)
    private Set<Vehicle> vehicles;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
