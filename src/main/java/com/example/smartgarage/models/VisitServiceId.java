package com.example.smartgarage.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VisitServiceId implements Serializable {
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "visit_id")
    private Visit visit;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisitServiceId that = (VisitServiceId) o;
        return visit == that.visit && service == that.service;
    }

    @Override
    public int hashCode() {
        return Objects.hash(visit, service);
    }
}
