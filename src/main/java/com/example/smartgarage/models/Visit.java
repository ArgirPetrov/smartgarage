package com.example.smartgarage.models;

import com.example.smartgarage.helpers.DoubleFormatter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "visits")
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;
    @Column(name = "is_finished")
    private boolean isFinished;
    @Column(name = "total_price")
    private double totalPrice;
    @org.hibernate.annotations.Generated(GenerationTime.ALWAYS)
    @Column(name = "date_of_visit")
    private LocalDate visitDate;
    @OneToMany(mappedBy = "id.visit", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<VisitServiceRelation> services;
    public void addVisitServiceRelation(VisitServiceRelation visitServiceRelation) {
        services.add(visitServiceRelation);
    }
    public void removeVisitServiceRelation(VisitServiceRelation visitServiceRelation) {
        services.remove(visitServiceRelation);
    }
    public void addToPrice(double servicePrice) {
        totalPrice = DoubleFormatter.roundToSecondDecimal(totalPrice + servicePrice);
    }
    public void removeFromPrice(double servicePrice) {
        totalPrice = DoubleFormatter.roundToSecondDecimal(totalPrice - servicePrice);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visit visit = (Visit) o;
        return id == visit.id;
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
