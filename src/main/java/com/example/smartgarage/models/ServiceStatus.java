package com.example.smartgarage.models;

public enum ServiceStatus {
    NOT_STARTED, IN_PROGRESS, DONE;

    @Override
    public String toString() {
        return switch (this) {
            case NOT_STARTED -> "Not Started";
            case IN_PROGRESS -> "In Progress";
            case DONE -> "Done";
        };
    }
    public static ServiceStatus getEnum(String value) {
        return switch (value) {
            case "Not Started" -> ServiceStatus.NOT_STARTED;
            case "In Progress" -> ServiceStatus.IN_PROGRESS;
            case "Done" -> ServiceStatus.DONE;
            default -> null;
        };
    }
}
