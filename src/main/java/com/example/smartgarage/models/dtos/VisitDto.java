package com.example.smartgarage.models.dtos;

import com.example.smartgarage.models.dtos.validations.groups.CreateViaParent;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class VisitDto {
    @NotNull(message = "Services cannot be null", groups = {CreateViaParent.class})
    private List<String> services;
    private Boolean isFinished;
}
