package com.example.smartgarage.models.dtos;

import com.example.smartgarage.models.dtos.validations.*;
import com.example.smartgarage.models.dtos.validations.groups.AuthenticateGroup;
import com.example.smartgarage.models.dtos.validations.groups.CreateGroup;
import com.example.smartgarage.models.dtos.validations.groups.UpdateGroup;
import lombok.*;
import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
@FieldsMatch(field = "password", fieldMatch = "confirmPassword", message = "Passwords need to match!",
groups = {UpdateGroup.class})
public class UserDto {
    @NotBlank(message = "Username cannot be empty!", groups = {AuthenticateGroup.class})
    private String username;
    @NotBlank(message = "Password cannot be empty!", groups = {AuthenticateGroup.class})
    @Size(min = 8, max = 20, message = "Password must be between 8 and 20 symbols",
            groups = {UpdateGroup.class})
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=])(?!.*\\s).+$",
            message = "Password must have at least one special symbol, one capital letter and one digit!",
            groups = {UpdateGroup.class})
    private String password;
    private String confirmPassword;
    @NotBlank(message = "Email cannot be empty!", groups = {CreateGroup.class})
    @Email(message = "Please provide an actual email!", groups = {CreateGroup.class})
    private String email;

    @Positive(message = "Must be a positive number!", groups = {CreateGroup.class})
    @ExactNumber(value = 10, message = "Phone number must be exactly 10 digits", groups = {CreateGroup.class})
    private long phoneNumber;
}
