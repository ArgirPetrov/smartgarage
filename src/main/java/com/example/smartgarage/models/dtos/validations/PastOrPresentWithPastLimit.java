package com.example.smartgarage.models.dtos.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {PastOrPresentWithPastLimitValidator.class})
public @interface PastOrPresentWithPastLimit {
    String message() default "Year must be in the present or past and after {min}";
    int min() default 0;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
