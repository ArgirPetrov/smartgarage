package com.example.smartgarage.models.dtos;

import com.example.smartgarage.models.dtos.validations.PastOrPresentWithPastLimit;
import com.example.smartgarage.models.dtos.validations.groups.CreateGroup;
import com.example.smartgarage.models.dtos.validations.groups.UpdateGroup;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.Year;

@Getter
@Setter
public class VehicleDto {
    @NotBlank(message = "License plate must not be empty", groups = {CreateGroup.class})
    private String licensePlate;
    @NotBlank(message = "VIN must not be empty", groups = {CreateGroup.class})
    private String VIN;
    @NotNull(message = "Year of creation must not be empty", groups = {CreateGroup.class})
    @PastOrPresentWithPastLimit(min = 1886, groups = {CreateGroup.class, UpdateGroup.class})
    private Year yearOfCreation;
    @NotBlank(message = "Model must not be empty", groups = {CreateGroup.class})
    private String model;
    @NotBlank(message = "Brand must not be empty", groups = {CreateGroup.class})
    private String brand;
    @NotBlank(message = "Username of customer must not be empty", groups = {CreateGroup.class})
    private String username;
}
