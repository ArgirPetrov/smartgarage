package com.example.smartgarage.models.dtos;


public record TokenResponseDto(
        String token, String expirationDate
) {

}
