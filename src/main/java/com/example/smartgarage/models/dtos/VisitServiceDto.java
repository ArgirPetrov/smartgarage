package com.example.smartgarage.models.dtos;

import com.example.smartgarage.models.ServiceStatus;
import com.example.smartgarage.models.dtos.validations.groups.UpdateGroup;
import com.example.smartgarage.models.dtos.validations.ValueOfEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VisitServiceDto {
    @ValueOfEnum(enumClass = ServiceStatus.class,
            groups = {UpdateGroup.class})
    private ServiceStatus status;
}
