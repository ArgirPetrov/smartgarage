package com.example.smartgarage.models.dtos.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = {ExactNumberValidator.class})
public @interface ExactNumber {
    String message() default "Must have exact value";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
    long value();
}
