package com.example.smartgarage.models.dtos.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ExactNumberValidator implements ConstraintValidator<ExactNumber, Object> {
    private long number;
    @Override
    public void initialize(ExactNumber constraintAnnotation) {
        number = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        return String.valueOf(value).length() == number;
    }
}
