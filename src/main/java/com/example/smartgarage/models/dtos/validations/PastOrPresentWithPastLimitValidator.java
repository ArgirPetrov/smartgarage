package com.example.smartgarage.models.dtos.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.Year;

public class PastOrPresentWithPastLimitValidator implements ConstraintValidator<PastOrPresentWithPastLimit, Year> {
    private int min;
    @Override
    public void initialize(PastOrPresentWithPastLimit constraintAnnotation) {
        min = constraintAnnotation.min();
    }

    @Override
    public boolean isValid(Year value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        return value.isAfter(Year.parse(String.valueOf(min))) && !value.isAfter(Year.now());
    }
}
