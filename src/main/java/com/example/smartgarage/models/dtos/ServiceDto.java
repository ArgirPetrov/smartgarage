package com.example.smartgarage.models.dtos;

import com.example.smartgarage.models.dtos.validations.groups.CreateGroup;
import com.example.smartgarage.models.dtos.validations.groups.UpdateGroup;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ServiceDto {
    @NotBlank(message = "Service name must not be empty", groups = {CreateGroup.class})
    @Size(min = 2, max = 32, message = "A service must be between 2 and 32 characters!",
            groups = {CreateGroup.class, UpdateGroup.class})
    private String service;
    @Positive(message = "Price must be a positive number!", groups = {CreateGroup.class})
    private double price;
}
