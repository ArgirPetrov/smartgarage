package com.example.smartgarage.helpers;

import javax.persistence.AttributeConverter;
import java.time.Year;

public class YearConverter implements AttributeConverter<Year, Integer> {
    @Override
    public Integer convertToDatabaseColumn(Year attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.getValue();
    }

    @Override
    public Year convertToEntityAttribute(Integer dbData) {
        if (dbData == null) {
            return null;
        }
        return Year.of(dbData);
    }
}
