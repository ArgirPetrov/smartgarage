package com.example.smartgarage.helpers;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class DoubleFormatter {
    private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");
    public static double roundToSecondDecimal(double doubleValue) {
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        String rounded = decimalFormat.format(doubleValue);
        return Double.parseDouble(rounded);
    }
}
