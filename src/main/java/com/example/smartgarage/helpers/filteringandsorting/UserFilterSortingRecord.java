package com.example.smartgarage.helpers.filteringandsorting;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Map;

import static com.example.smartgarage.helpers.filteringandsorting.FilteringHelpers.*;

public record UserFilterSortingRecord(
        String username, String email, String phoneNumber, String makeOrModel, LocalDate visitBegin,
        LocalDate visitEnd, Pageable pageable
) {
    public static UserFilterSortingRecord createUserFilterSortingRecord(Map<String, String> params, Pageable pageable) {
        String username = extractStringFilterParam("username", params);
        String email = extractStringFilterParam("email", params);
        String makeOrModel = extractStringFilterParam("vehicle", params);


        String phoneNumber = extractStringFilterParam("phone-number", params);
        LocalDate visitBegin = extractLocalDateFilterParam("start-date", params);
        LocalDate visitEnd = extractLocalDateFilterParam("end-date", params);

        return new UserFilterSortingRecord(username, email, phoneNumber, makeOrModel, visitBegin, visitEnd,
                extractPageable(pageable, params));
    }
    private static Pageable extractPageable(Pageable pageable, Map<String, String> params) {
        String property = params.containsKey("sort") && (params.get("sort").equalsIgnoreCase("username") ||
                    params.get("sort").equalsIgnoreCase("date")) ? params.get("sort") : "id";
        Sort.Direction direction = getDirection(params);

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(direction, property));
    }
}
