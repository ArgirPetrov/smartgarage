package com.example.smartgarage.helpers.filteringandsorting;


import com.example.smartgarage.models.User;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.Year;
import java.util.Map;

import static com.example.smartgarage.helpers.filteringandsorting.FilteringHelpers.*;

public record VehicleFilterSortingRecord(
        String brand, String model, Year year, String ownerUsername, Pageable pageable
){
    public static VehicleFilterSortingRecord createVehicleFilterSortingRecord(Map<String, String> params,
                                                                           Pageable pageable) {
        String brand = extractStringFilterParam("brand", params);
        String model = extractStringFilterParam("model", params);
        Year year = extractYearFilterParam("year", params);
        String ownerUsername = extractStringFilterParam("username", params);

        return new VehicleFilterSortingRecord(brand, model, year, ownerUsername,
                extractPageable(pageable, params));
    }
    private static Pageable extractPageable(Pageable pageable, Map<String, String> params) {
        String property = params.containsKey("sort") &&
                (params.get("sort").equalsIgnoreCase("model") ||
                params.get("sort").equalsIgnoreCase("brand") ||
                params.get("sort").equalsIgnoreCase("year")) ? params.get("sort") : "id";
        Sort.Direction direction = getDirection(params);

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(direction, property));
    }
}
