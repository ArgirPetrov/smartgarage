package com.example.smartgarage.helpers.filteringandsorting;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Map;

public class FilteringHelpers {
    public static LocalDate extractLocalDateFilterParam(String filterParam, Map<String, String> params) {
        try {
            return params.get(filterParam) != null && !params.get(filterParam).isEmpty() ?
                    LocalDate.parse(params.get(filterParam)) : null;
        } catch (DateTimeParseException e) {
            return filterParam.equals("start-date") ? LocalDate.now().plusDays(1) : LocalDate.MIN;
        }
    }

    public static String extractStringFilterParam(String filterParam, Map<String, String> params) {
        return params.get(filterParam) != null && !params.get(filterParam).isEmpty() ?
                params.get(filterParam) : null;
    }
    public static Double extractDoubleFilterParam(String filterParam, Map<String, String> params) {
        try {
            return params.get(filterParam) != null && !params.get(filterParam).isEmpty() ?
                    Double.parseDouble(params.get(filterParam)) : null;
        } catch (NumberFormatException e) {
            return filterParam.equals("start-price") ? (double) Integer.MAX_VALUE : (double) Integer.MIN_VALUE;
        }
    }
    public static Year extractYearFilterParam(String filterParam, Map<String, String> params) {
        try {
            return params.get(filterParam) != null && !params.get(filterParam).isEmpty() ?
                    Year.parse(params.get(filterParam)) : null;
        } catch (DateTimeParseException e) {
            return Year.of(Year.MAX_VALUE);
        }
    }
    public static Sort.Direction getDirection(Map<String, String> params) {
        return params.containsKey("order") && params.get("order").equalsIgnoreCase("desc") ?
                Sort.Direction.DESC : Sort.Direction.ASC;
    }
    public static Pageable generateBasicPageable(Pageable pageable) {
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                Sort.by(Sort.Direction.ASC, "id"));
    }
    public static String modifySearchKeyword(String keyword) {
        if (keyword != null && keyword.isBlank()) return null;
        return keyword;
    }
}
