package com.example.smartgarage.helpers.filteringandsorting;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Map;

import static com.example.smartgarage.helpers.filteringandsorting.FilteringHelpers.*;

public record VisitFilterSortingRecord(
        String userId, String vehicleId, LocalDate visitBegin,
        LocalDate visitEnd, Pageable pageable
) {
    public static VisitFilterSortingRecord createVisitFilterSortingRecord(Map<String, String> params, Pageable pageable) {
        String userId  = extractStringFilterParam("userId", params);
        String vehicleId = extractStringFilterParam("vehicleId", params);

        LocalDate visitBegin = extractLocalDateFilterParam("start-date", params);
        LocalDate visitEnd = extractLocalDateFilterParam("end-date", params);

        return new VisitFilterSortingRecord(userId, vehicleId, visitBegin, visitEnd,
                extractPageable(params, pageable));
    }
    public static Pageable extractPageable(Map<String, String> params, Pageable pageable) {
        Sort.Direction direction = getDirection(params);
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(direction, "id"));
    }
}
