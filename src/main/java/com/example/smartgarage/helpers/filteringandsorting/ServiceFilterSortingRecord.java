package com.example.smartgarage.helpers.filteringandsorting;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import java.util.Map;

import static com.example.smartgarage.helpers.filteringandsorting.FilteringHelpers.*;
import static com.example.smartgarage.helpers.filteringandsorting.FilteringHelpers.extractDoubleFilterParam;

public record ServiceFilterSortingRecord(
        String name, Double priceMin, Double priceMax, Pageable pageable
) {
    public static ServiceFilterSortingRecord createServiceFilterSortingRecord(Map<String, String> params,
                                                                              Pageable pageable) {
        String name = extractStringFilterParam("name", params);
        Double priceMin = extractDoubleFilterParam("start-price", params);
        Double priceMax = extractDoubleFilterParam("end-price", params);
        return new ServiceFilterSortingRecord(name, priceMin, priceMax, extractPageable(params, pageable));
    }
    private static Pageable extractPageable(Map<String, String> params, Pageable pageable) {
        String property = params.containsKey("sort") && (params.get("sort").equalsIgnoreCase("name") ||
                params.get("sort").equalsIgnoreCase("price")) ? params.get("sort") : "id";
        Sort.Direction direction = getDirection(params);

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(direction, property));
    }
}
