package com.example.smartgarage.helpers;

import com.example.smartgarage.models.*;
import com.example.smartgarage.models.dtos.*;
import com.example.smartgarage.services.contracts.*;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.smartgarage.helpers.RandomStringGenerator.randomStringGenerator;
import static com.example.smartgarage.helpers.RandomStringGenerator.usernameGenerator;

@Component
@RequiredArgsConstructor
public class ModelMapper {
    private final UserService userService;
    private final ServiceService serviceService;
    private final ModelService modelService;
    private final VisitService visitService;
    private final VehicleService vehicleService;
    private final VisitServiceRelationService visitServiceRelationService;

    public User dtoToObject(UserDto userDto) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setUsername(usernameGenerator(userDto.getEmail()));
        user.setPassword(randomStringGenerator());
        return user;
    }
    public Visit dtoToObject(int vehicleId, int visitId, VisitDto visitDto) {
        Visit visit = visitService.getVisitByIdAndVehicleId(vehicleId, visitId);
        if (visitDto.getIsFinished() != null) visit.setFinished(visitDto.getIsFinished());
        return visit;
    }
    public Service dtoToObject(ServiceDto serviceDto) {
        Service service = new Service();
        service.setName(serviceDto.getService());
        service.setPrice(serviceDto.getPrice());

        return service;
    }
    public Vehicle dtoToObject(VehicleDto vehicleDto) {
        Vehicle vehicle = new Vehicle();
        vehicle.setOwner(userService.getByUsername(vehicleDto.getUsername()));
        vehicle.setVin(vehicleDto.getVIN());
        vehicle.setLicensePlate(vehicleDto.getLicensePlate());
        vehicle.setYear(vehicleDto.getYearOfCreation());
        vehicle.setModel(modelService.createModelIfNotExit(vehicleDto.getModel(), vehicleDto.getBrand()));

        return vehicle;
    }
    public Vehicle dtoToObject(int id, VehicleDto vehicleDto) {
        Vehicle vehicle = vehicleService.getById(id);

        if (vehicleDto.getVIN() != null && !vehicleDto.getVIN().isBlank())
            vehicle.setVin(vehicleDto.getVIN());
        if (vehicleDto.getLicensePlate() != null && !vehicleDto.getLicensePlate().isBlank())
            vehicle.setLicensePlate(vehicleDto.getLicensePlate());
        if (vehicleDto.getYearOfCreation() != null)
            vehicle.setYear(vehicleDto.getYearOfCreation());
        if (vehicleDto.getModel() != null && !vehicleDto.getModel().isBlank() &&
                vehicleDto.getBrand() != null && !vehicleDto.getBrand().isBlank()) {
            vehicle.setModel(modelService.createModelIfNotExit(vehicleDto.getModel(), vehicleDto.getBrand()));
        } else if (vehicleDto.getModel() != null && !vehicleDto.getModel().isBlank()) {
            vehicle.setModel(modelService.createModelIfNotExit(vehicleDto.getModel(),
                    vehicle.getModel().getBrand().getName()));
        } else if (vehicleDto.getBrand() != null && !vehicleDto.getBrand().isBlank()){
            vehicle.setModel(modelService.createModelIfNotExit(vehicle.getModel().getName(),
                    vehicleDto.getBrand()));
        }

        return vehicle;
    }
    public User dtoToObject(int id, UserDto userDto) {
        User user = userService.getById(id);
        if (userDto.getPassword() != null && !userDto.getPassword().isBlank())
            user.setPassword(userDto.getPassword());
        return user;
    }
    public Service dtoToObject(int id, ServiceDto serviceDto) {
        Service service = serviceService.getById(id);
        if (serviceDto.getService() != null && !serviceDto.getService().isBlank())
            service.setName(serviceDto.getService());
        if (serviceDto.getPrice() > 0) service.setPrice(serviceDto.getPrice());
        return service;
    }

    public Visit dtoToObject(VisitDto visitDto) {
        Visit visit = new Visit();
        if (visitDto != null && visitDto.getServices() != null) {
            visit.setServices(createVisitServiceRelations(visit, visitDto));
            visit.setTotalPrice(visit.getServices().stream().map(VisitServiceRelation::getServicePrice)
                    .reduce(0.0, Double::sum));
        }
        return visit;
    }
    public Set<VisitServiceRelation> createVisitServiceRelations(Visit visit, VisitDto visitDto) {
        return visitDto.getServices().stream().filter(serviceService::serviceExist)
                .map(serviceService::getByName).map(log ->
                        new VisitServiceRelation(new VisitServiceId(visit, log),
                                log.getName(),
                                log.getPrice()))
                .collect(Collectors.toSet());
    }

    public VisitServiceRelation dtoToObject(int vehicleId, int visitId, int serviceId,
                                            VisitServiceDto visitServiceDto) {
        Visit visit = visitService.getVisitByIdAndVehicleId(vehicleId, visitId);
        VisitServiceRelation visitServiceRelation = visitServiceRelationService
                .getByVisitAndServiceId(visit, serviceId);
        if (visitServiceDto.getStatus() != null) visitServiceRelation.setStatus(visitServiceDto.getStatus());

        return visitServiceRelation;
    }
}
