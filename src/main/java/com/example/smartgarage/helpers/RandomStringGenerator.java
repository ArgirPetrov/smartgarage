package com.example.smartgarage.helpers;

import java.util.Random;

public class RandomStringGenerator {
    private static final int DEFAULT_STRING_LENGTH = 20;
    public static String usernameGenerator(String email) {
        int endIndex = Math.min(email.indexOf("@"), 10);
        return email.substring(0, endIndex) + randomStringGenerator(10);
    }
    public static String randomStringGenerator(int length) {
        return new Random().ints(48, 123)
                .filter(i -> (i >= 65 && i <= 90) || (i <= 122 && i >= 97)).limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
    }
    public static String randomStringGenerator() {
        return new Random().ints(48, 123)
                .filter(i -> (i >= 65 && i <= 90) || (i <= 122 && i >= 97)).limit(DEFAULT_STRING_LENGTH)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
    }


}
