package com.example.smartgarage.controllers;

import com.example.smartgarage.exceptionss.EntityDuplicateException;
import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.helpers.ModelMapper;
import com.example.smartgarage.models.Service;
import com.example.smartgarage.models.dtos.ServiceDto;
import com.example.smartgarage.models.dtos.validations.groups.CreateGroup;
import com.example.smartgarage.models.dtos.validations.groups.UpdateGroup;
import com.example.smartgarage.services.contracts.ServiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/services")
@RequiredArgsConstructor
public class ServiceRestController {
    private final ServiceService serviceService;
    private final ModelMapper modelMapper;
    @GetMapping
    public List<Service> getAll(@PageableDefault(sort = "id")Pageable pageable,
                                @RequestParam Map<String, String> params) {
        return serviceService.getAll(pageable, params);
    }
    @GetMapping("/search")
    public List<Service> search(@PageableDefault(sort = "id") Pageable pageable,
                                @RequestParam(required = false) String q) {
        return serviceService.search(pageable, q);
    }
    @GetMapping("/{id}")
    private Service getById(@PathVariable int id) {
        try {
            return serviceService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PostMapping
    public Service create(@RequestBody @Validated(CreateGroup.class) ServiceDto serviceDto) {
        try {
            Service service = modelMapper.dtoToObject(serviceDto);
            serviceService.create(service);
            return service;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public Service update(@PathVariable int id,
                          @RequestBody @Validated(UpdateGroup.class) ServiceDto serviceDto) {
        try {
            Service service = modelMapper.dtoToObject(id, serviceDto);
            serviceService.update(service);
            return service;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            Service service = serviceService.getById(id);
            serviceService.delete(service);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
