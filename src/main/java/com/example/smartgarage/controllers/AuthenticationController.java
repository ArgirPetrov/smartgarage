package com.example.smartgarage.controllers;

import com.example.smartgarage.config.JwtUtils;
import com.example.smartgarage.helpers.DateFormatter;

import com.example.smartgarage.models.CustomUserDetails;
import com.example.smartgarage.models.dtos.TokenResponseDto;
import com.example.smartgarage.models.dtos.UserDto;
import com.example.smartgarage.models.dtos.validations.groups.AuthenticateGroup;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    @PostMapping("/token")
    public TokenResponseDto generateToken(@Validated(AuthenticateGroup.class)
                                             @RequestBody UserDto authenticate) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticate.getUsername(), authenticate.getPassword())
        );
        CustomUserDetails user = (CustomUserDetails) authentication.getPrincipal();
        String token = jwtUtils.generateToken(user);
        Date date = jwtUtils.extractExpiration(token);
        return new TokenResponseDto(token, DateFormatter.formatToString(date));
    }
}
