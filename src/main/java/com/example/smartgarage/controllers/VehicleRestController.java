package com.example.smartgarage.controllers;

import com.example.smartgarage.exceptionss.EntityDuplicateException;
import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.helpers.ModelMapper;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.dtos.VehicleDto;
import com.example.smartgarage.models.dtos.VisitDto;
import com.example.smartgarage.models.dtos.VisitServiceDto;
import com.example.smartgarage.models.dtos.validations.groups.CreateGroup;
import com.example.smartgarage.models.dtos.validations.groups.CreateViaParent;
import com.example.smartgarage.models.dtos.validations.groups.UpdateGroup;
import com.example.smartgarage.services.contracts.VehicleService;
import com.example.smartgarage.services.contracts.VisitService;
import com.example.smartgarage.services.contracts.VisitServiceRelationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/vehicles")
public class VehicleRestController {
    private final VehicleService vehicleService;
    private final VisitServiceRelationService visitServiceRelationService;
    private final ModelMapper modelMapper;
    private final VisitService visitService;
    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<Vehicle> getAll(@PageableDefault(sort = "id") Pageable pageable,
                                @RequestParam Map<String, String> params) {
        return vehicleService.getAll(pageable, params);
    }
    @GetMapping("/search")
    @PreAuthorize("hasRole('ADMIN')")
    public List<Vehicle> search(@PageableDefault(sort = "id") Pageable pageable,
                                @RequestParam(required = false) String q) {
        return vehicleService.search(pageable, q);
    }
    @GetMapping("/{id}")
    @PostAuthorize("@securityService.isOwner(#id) or hasRole('ADMIN')")
    public Vehicle getById(@PathVariable int id) {
        try {
            return vehicleService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PostMapping
    public Vehicle create(@RequestBody @Validated(CreateGroup.class) VehicleDto vehicleDto) {
        try {
            Vehicle vehicle = modelMapper.dtoToObject(vehicleDto);
            vehicleService.create(vehicle);
            return vehicle;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public Vehicle update(@PathVariable int id,
                          @RequestBody @Validated(UpdateGroup.class) VehicleDto vehicleDto) {
        try {
            Vehicle vehicle = modelMapper.dtoToObject(id, vehicleDto);
            vehicleService.update(vehicle);
            return vehicle;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            Vehicle vehicle = vehicleService.getById(id);
            vehicleService.delete(vehicle);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @GetMapping("/{id}/visits")
    @PostAuthorize("@securityService.isOwner(id) or hasRole('ADMIN')")
    public List<Visit> getAllVisitsOfVehicle(@PathVariable int id, @RequestParam Map<String, String> params,
                                             @PageableDefault(sort = "id") Pageable pageable) {
        try {
            Vehicle vehicle = vehicleService.getById(id);
            params.put("vehicleId", String.valueOf(vehicle.getId()));
            return visitService.getAll(pageable, params);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @GetMapping("/{vehicleId}/visits/{visitId}")
    @PostAuthorize("@securityService.isOwner(vehicleId) or hasRole('ADMIN')")
    public Visit getVisitByIdAndVehicleId(@PathVariable int vehicleId, @PathVariable int visitId) {
        try {
            return visitService.getVisitByIdAndVehicleId(vehicleId, visitId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PostMapping("/{id}/visits")
    public Visit createVisit(@PathVariable int id,
                                   @RequestBody(required = false) VisitDto visitDto) {
        try {
            Vehicle vehicle = vehicleService.getById(id);
            Visit visit = modelMapper.dtoToObject(visitDto);
            visit.setVehicle(vehicle);
            visitService.create(visit);
            return visit;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    @PutMapping("/{vehicleId}/visits/{visitId}")
    public Visit updateVisit(@PathVariable int vehicleId, @PathVariable int visitId,
                             @RequestBody VisitDto visitDto) {
        try {
            Visit visit = modelMapper.dtoToObject(vehicleId, visitId, visitDto);
            visitService.update(visit);
            return visit;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PutMapping("/{vehicleId}/visits/{visitId}/services")
    public Visit addVisitServices(@PathVariable int vehicleId, @PathVariable int visitId,
                               @RequestBody @Validated(CreateViaParent.class) VisitDto visitDto) {
        try {
            Visit visitFromRepo = visitService.getVisitByIdAndVehicleId(vehicleId, visitId);
            Set<VisitServiceRelation> visitServiceRelations = modelMapper
                    .createVisitServiceRelations(visitFromRepo, visitDto);
            visitService.addServices(visitFromRepo, visitServiceRelations);
            return visitFromRepo;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @DeleteMapping("/{vehicleId}/visits/{visitId}/services")
    public Visit removeVisitServices(@PathVariable int vehicleId, @PathVariable int visitId,
                             @RequestBody @Validated(CreateViaParent.class) VisitDto visitDto) {
        try {
            Visit visitFromRepo = visitService.getVisitByIdAndVehicleId(vehicleId, visitId);
            Set<VisitServiceRelation> visitServiceRelations = modelMapper
                    .createVisitServiceRelations(visitFromRepo, visitDto);
            visitService.removeServices(visitFromRepo, visitServiceRelations);
            return visitFromRepo;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{vehicleId}/visits/{visitId}/services/{serviceId}")
    public VisitServiceRelation updateVisitService(@PathVariable int vehicleId, @PathVariable int visitId,
                                     @PathVariable int serviceId,
                                     @Validated(UpdateGroup.class) @RequestBody VisitServiceDto visitServiceDto) {
        try {
            VisitServiceRelation visitServiceRelation = modelMapper.dtoToObject(vehicleId, visitId,
                    serviceId, visitServiceDto);
            visitServiceRelationService.update(visitServiceRelation);
            return visitServiceRelation;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
