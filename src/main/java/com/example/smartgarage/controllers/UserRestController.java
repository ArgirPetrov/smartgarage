package com.example.smartgarage.controllers;

import com.example.smartgarage.exceptionss.EntityDuplicateException;
import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.helpers.ModelMapper;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.dtos.UserDto;
import com.example.smartgarage.models.dtos.validations.groups.CreateGroup;
import com.example.smartgarage.models.dtos.validations.groups.UpdateGroup;
import com.example.smartgarage.services.contracts.UserService;
import com.example.smartgarage.services.contracts.VehicleService;
import com.example.smartgarage.services.contracts.VisitService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserRestController {
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final VehicleService vehicleService;
    private final VisitService visitService;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getAll(@PageableDefault(sort = "id") Pageable pageable,
                             @RequestParam Map<String, String> params) {
        return userService.getAll(pageable, params);
    }
    @GetMapping("/search")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> search(@PageableDefault(sort = "id") Pageable pageable,
                             @RequestParam(required = false) String q) {
        return userService.search(pageable, q);
    }
    @GetMapping("/{id}")
    @PostAuthorize("#id == authentication.principal.getUser().getId() or hasRole('ADMIN')")
    public User getById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PostMapping
    public User create(@RequestBody @Validated(CreateGroup.class) UserDto userDto) {
        try {
            User user = modelMapper.dtoToObject(userDto);
            userService.create(user);
            return user;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public User update(@PathVariable int id, @RequestBody @Validated(UpdateGroup.class) UserDto userDto) {
        try {
            User user = modelMapper.dtoToObject(id, userDto);
            userService.update(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            User user = userService.getById(id);
            userService.delete(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @GetMapping("/{id}/vehicles")
    @PostAuthorize("#id == authentication.principal.getUser().getId() or hasRole('ADMIN')")
    public List<Vehicle> getVehiclesByUserId(@PathVariable int id,
                                             @PageableDefault(sort = "id") Pageable pageable,
                                             @RequestParam Map<String, String> params) {
        try {
            User user = userService.getById(id);
            params.put("username", user.getUsername());
            return vehicleService.getAll(pageable, params);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @GetMapping("/{id}/visits")
    @PostAuthorize("#id == authentication.principal.getUser().getId() or hasRole('ADMIN')")
    public List<Visit> getVisitsByUserId(@PathVariable int id,
                                         @PageableDefault(sort = "id") Pageable pageable,
                                         @RequestParam Map<String, String> params) {
        try {
            User user = userService.getById(id);
            params.put("userId", String.valueOf(user.getId()));
            return visitService.getAll(pageable, params);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
