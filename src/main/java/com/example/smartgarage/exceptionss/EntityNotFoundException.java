package com.example.smartgarage.exceptionss;

public class EntityNotFoundException extends RuntimeException{
    private static final String NOT_FOUND_MESSAGE = "%s with %s %s is not found!";
    public EntityNotFoundException(String message) {
        super(message);
    }
    public EntityNotFoundException(String entity, String attribute, String value) {
        super(String.format(NOT_FOUND_MESSAGE, entity, attribute, value));
    }
    public EntityNotFoundException(String entity, int id) {
        this(entity, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String subEntity, int subEntityId, String superEntity, int superEntityId) {
        super(String.format("%s with id %d for %s with id %d is not found!",
                subEntity, subEntityId, superEntity, superEntityId));
    }

}
