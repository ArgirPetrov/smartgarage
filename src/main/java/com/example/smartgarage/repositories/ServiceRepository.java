package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.models.Service;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ServiceRepository extends JpaRepository<Service, Integer> {
    Optional<Service> findByName(String name);
    default Service getByName(String name) {
        return findByName(name).orElseThrow(() -> new EntityNotFoundException("Service", "name", name));
    }
    default Service getById(int id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException("Service", id));
    }
    @Query("from Service where (:name is null or name like %:name%) " +
            "and (:priceMin is null or price >= :priceMin) " +
            "and (:priceMax is null or price <= :priceMax) and isDeleted = false")
    List<Service> getAll(String name, Double priceMin, Double priceMax, Pageable pageable);
    @Query("from Service where :keyword is not null and (name like %:keyword% or CONCAT(price, '') = :keyword) " +
            "and isDeleted = false")
    List<Service> search(Pageable pageable, String keyword);
    boolean existsByName(String name);
}
