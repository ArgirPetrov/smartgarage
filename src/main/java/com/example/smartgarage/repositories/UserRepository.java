package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.models.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("from User u left join u.vehicles as uv left join uv.model m left join m.brand b left join uv.visits as uvv " +
            "where (:username is null or u.username like %:username%) " +
            "and (:phoneNumber is null or CONCAT(u.phoneNumber, '') like %:phoneNumber%) " +
            "and (:email is null or u.email like %:email%) " +
            "and (:makeOrModel is null or (m.name like %:makeOrModel% or b.name like %:makeOrModel%))" +
            "and (:visitBegin is null or uvv.visitDate >= :visitBegin) " +
            "and (:visitEnd is null or uvv.visitDate <= :visitEnd) and u.isDeleted = false group by u.id")
    List<User> getAll(Pageable pageable, String username, String phoneNumber, String email,
                      String makeOrModel, LocalDate visitBegin, LocalDate visitEnd);
    @Query("from User u left join u.vehicles as uv " +
            "where :keyword is not null and (u.username like %:keyword% or u.email like %:keyword% or " +
            "uv.licensePlate like %:keyword% or uv.vin like %:keyword% or CONCAT(u.phoneNumber, '') like " +
            "%:keyword%) and u.isDeleted = false")
    List<User> search(Pageable pageable, String keyword);
    default User getById(int id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException("User", id));
    }
    Optional<User> findByUsername(String username);
    default User getByUsername(String username) {
        return findByUsername(username).orElseThrow(() -> new EntityNotFoundException("User", "username", username));
    }
    Optional<User> findByEmail(String email);
    default User getByEmail(String email) {
        return findByEmail(email).orElseThrow(() -> new EntityNotFoundException("User", "email", email));
    }
    Optional<User> findByPhoneNumber(long phoneNumber);
    default User getByPhoneNumber(long phoneNumber) {
        return findByPhoneNumber(phoneNumber).orElseThrow(() ->
                new EntityNotFoundException("User", "phone number", String.valueOf(phoneNumber)));
    }
}
