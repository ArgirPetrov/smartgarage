package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.models.Brand;
import com.example.smartgarage.models.Model;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ModelRepository extends JpaRepository<Model, Integer> {
    Optional<Model> findByBrandAndName(Brand brand, String modelName);
    default Model getByBrandAndName(Brand brand, String modelName) {
        return findByBrandAndName(brand, modelName).orElseThrow(() ->
                new EntityNotFoundException("Model %s for brand %s does not exist", modelName, brand.getName()));
    }
}
