package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.Visit;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface VisitRepository extends JpaRepository<Visit, Integer> {

    default Visit getById(int id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException("Visit", id));
    }
    Optional<Visit> findByIdAndVehicle(int visitId, Vehicle vehicle);
    default Visit getByIdAndVehicle(int visitId, Vehicle vehicle) {
        return findByIdAndVehicle(visitId, vehicle).orElseThrow(() ->
                new EntityNotFoundException("Visit", visitId, "vehicle", vehicle.getId()));
    }
    @Query("from Visit v left join v.vehicle vv left join vv.owner vvo " +
            "where (:userId is null or CONCAT(vvo.id, '') = :userId) " +
            "and (:vehicleId is null or CONCAT(vv.id, '') = :vehicleId) " +
            "and (:visitBegin is null or v.visitDate >= :visitBegin) " +
            "and (:visitEnd is null or v.visitDate <= :visitEnd) group by v.id")
    List<Visit> getAll(String userId, String vehicleId, LocalDate visitBegin, LocalDate visitEnd, Pageable pageable);
}
