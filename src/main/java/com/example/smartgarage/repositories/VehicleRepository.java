package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.Visit;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.Year;
import java.util.List;
import java.util.Optional;

public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {
    @Query("from Vehicle v  " +
            "where (:model is null or v.model.name like %:model%) " +
            "and (:brand is null or v.model.brand.name like %:brand%) " +
            "and (:year is null or v.year = :year) " +
            "and (:owner is null or v.owner.username like %:owner%)")
    List<Vehicle> getAll(Pageable pageable, String model, String brand, Year year, String owner);

    default Vehicle getById(int id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException("Vehicle", id));
    }
    Optional<Vehicle> findByLicensePlate(String licensePlate);
    default Vehicle getByLicensePlate(String licensePlate) {
        return findByLicensePlate(licensePlate).orElseThrow(() ->
                new EntityNotFoundException("Vehicle", "license plate", licensePlate));
    }
    Optional<Vehicle> findByVin(String vin);
    default Vehicle getByVin(String vin) {
        return findByVin(vin).orElseThrow(() ->
                new EntityNotFoundException("Vehicle", "VIN", vin));
    }
    @Query("from Vehicle v left join v.owner as vo " +
            "where :keyword is not null and (v.licensePlate like %:keyword% or v.vin like %:keyword% or " +
            "CONCAT(vo.phoneNumber, '') like %:keyword%)")
    List<Vehicle> search(Pageable pageable, String keyword);
    void deleteAllByOwner(User user);
}
