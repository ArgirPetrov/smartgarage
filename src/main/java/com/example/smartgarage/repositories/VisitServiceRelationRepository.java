package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptionss.EntityNotFoundException;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitServiceId;
import com.example.smartgarage.models.VisitServiceRelation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VisitServiceRelationRepository extends JpaRepository<VisitServiceRelation, VisitServiceId> {
    default VisitServiceRelation getByVisitAndServiceId(Visit visit, int serviceId) {
        return findVisitServiceRelationById_VisitAndId_Service_Id(visit, serviceId)
                .orElseThrow(() -> new EntityNotFoundException("Service", serviceId, "visit", visit.getId()));
    }
    Optional<VisitServiceRelation> findVisitServiceRelationById_VisitAndId_Service_Id(Visit visit, int serviceId);
}
