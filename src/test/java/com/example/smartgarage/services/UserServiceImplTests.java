package com.example.smartgarage.services;

import com.example.smartgarage.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.Assert;


import java.util.List;
import java.util.Map;

import static com.example.smartgarage.helpers.Utils.mockPageable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {
    @Mock
    UserRepository mockUserRepository;
    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getAll_Should_CallRepository_When_NoSortParameter() {
        // Arrange
        Pageable pageable = mockPageable();
        Mockito.when(mockUserRepository.getAll(any(), any(), any(), any(), any(), any(), any()))
                .thenReturn(List.of());
        // Act
        userService.getAll(pageable, Map.of());
        // Assert
        Mockito.verify(mockUserRepository, times(1))
                .getAll(any(), any(), any(), any(), any(), any(), any());
    }

    @ParameterizedTest
    @ValueSource(strings = {"username", "randomText", "id", "date", ""})
    public void getAll_Should_CallRepository_When_SortByAny(String sortType) {
        // Arrange
        Pageable pageable = mockPageable();
        Mockito.when(mockUserRepository.getAll(any(), any(), any(), any(), any(), any(), any()))
                .thenReturn(List.of());
        // Act
        userService.getAll(pageable, Map.of("sort", sortType));
        // Assert
        Mockito.verify(mockUserRepository, times(1))
                .getAll(any(), any(), any(), any(), any(), any(), any());
    }

}
