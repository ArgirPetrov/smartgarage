package com.example.smartgarage.helpers;

import com.example.smartgarage.models.User;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;

public class Utils {

    public static User mockUser() {
        User user = new User();
        user.setId(1);
        user.setUsername("mockUsername");
        user.setPassword("mockPassword");
        user.setEmail("mockEmail@email.com");
        user.setPhoneNumber(1234567891L);
        user.setDateCreated(LocalDateTime.now());

        return user;
    }
    public static Pageable mockPageable() {
        return PageRequest.of(1, 1, Sort.by(Sort.Direction.ASC, "id"));
    }
}
