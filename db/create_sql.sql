create or replace table brands
(
    id   int auto_increment
        primary key,
    name varchar(24) not null,
    constraint brands_pk2
        unique (name)
);

create or replace table models
(
    id       int auto_increment
        primary key,
    brand_id int         not null,
    name     varchar(24) not null,
    constraint models_pk
        unique (brand_id, name),
    constraint models_brands_id_fk
        foreign key (brand_id) references brands (id)
);

create or replace table services
(
    id         int auto_increment
        primary key,
    name       varchar(32)          not null,
    price      double               not null,
    is_deleted tinyint(1) default 0 not null,
    constraint services_pk2
        unique (name)
);

create or replace table users
(
    id           int auto_increment
        primary key,
    username     varchar(64)                           not null,
    password     varchar(64)                           not null,
    email        varchar(32)                           not null,
    phone_number bigint                                not null,
    date_created timestamp default current_timestamp() not null,
    constraint users_pk2
        unique (username),
    constraint users_pk3
        unique (email),
    constraint users_pk4
        unique (phone_number)
);

create or replace table permissions
(
    user_id     int                  not null
        primary key,
    is_deleted  tinyint(1) default 0 null,
    is_employee tinyint(1) default 0 not null,
    is_blocked  tinyint(1) default 0 not null,
    constraint permissions_users_id_fk
        foreign key (user_id) references users (id)
);

create or replace table vehicles
(
    id            int auto_increment
        primary key,
    license_plate varchar(8)  not null,
    VIN           varchar(17) not null,
    year_creation smallint    not null,
    model_id      int         not null,
    owner_id      int         null,
    constraint vehicles_pk
        unique (VIN),
    constraint vehicles_pk2
        unique (license_plate),
    constraint vehicles_models_id_fk
        foreign key (model_id) references models (id),
    constraint vehicles_users_id_fk
        foreign key (owner_id) references users (id)
);

create or replace table visits
(
    id            int auto_increment
        primary key,
    vehicle_id    int                                    not null,
    is_finished   tinyint(1) default 0                   not null,
    total_price   double     default 0                   not null,
    date_of_visit timestamp  default current_timestamp() not null,
    constraint visits_vehicles_id_fk
        foreign key (vehicle_id) references vehicles (id)
);

create or replace table visits_services
(
    visit_id               int                                                                     not null,
    service_id             int                                                                     not null,
    original_service_name  varchar(64)                                                             not null,
    original_service_price double                                                                  not null,
    service_status         enum ('NOT_STARTED', 'IN_PROGRESS', 'DONE') default 'NOT_STARTED'       not null,
    date_created           timestamp                                   default current_timestamp() not null,
    primary key (visit_id, service_id),
    constraint visits_services_services_id_fk
        foreign key (service_id) references services (id),
    constraint visits_services_visits_id_fk
        foreign key (visit_id) references visits (id)
);

